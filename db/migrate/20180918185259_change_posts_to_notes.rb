# frozen_string_literal: true

class ChangePostsToNotes < ActiveRecord::Migration[5.2]
  def change
    rename_table :posts, :notes
  end
end
