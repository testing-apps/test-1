# frozen_string_literal: true

class CreateSolutions < ActiveRecord::Migration[5.2]
  def change
    create_table :solutions do |t|
      t.text :body
      t.references :note_roadblock, foreign_key: true, on_delete: :cascade
      t.references :users, foreign_key: true
      t.timestamps
    end
  end
end
