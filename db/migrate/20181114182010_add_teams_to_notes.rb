# frozen_string_literal: true

class AddTeamsToNotes < ActiveRecord::Migration[5.2]
  def change
    add_reference :notes, :teams, foreign_key: true
  end
end
