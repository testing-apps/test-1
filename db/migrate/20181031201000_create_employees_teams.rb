# frozen_string_literal: true

class CreateEmployeesTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :employees_teams do |t|
      t.references :employees, foreign_key: true
      t.references :teams, foreign_key: true
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
