# frozen_string_literal: true

class ChangeColumnDefault < ActiveRecord::Migration[5.2]
  def change
    change_column_default :users, :password_digest, from: nil, to: BCrypt::Password.create('password')
  end
end
