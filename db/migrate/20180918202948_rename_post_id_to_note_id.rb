# frozen_string_literal: true

class RenamePostIdToNoteId < ActiveRecord::Migration[5.2]
  def change
    rename_column :comments, :post_id, :note_id
  end
end
