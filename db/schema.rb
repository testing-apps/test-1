# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_19_212156) do

  create_table "comments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.text "body"
    t.bigint "note_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "users_id"
    t.index ["note_id"], name: "index_comments_on_note_id"
    t.index ["users_id"], name: "index_comments_on_users_id"
  end

  create_table "employees", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "first_name", limit: 40, null: false
    t.string "last_name", limit: 40, null: false
    t.string "email_address", limit: 80
    t.bigint "users_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["users_id"], name: "index_employees_on_users_id"
  end

  create_table "employees_teams", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "employees_id"
    t.bigint "teams_id"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employees_id"], name: "index_employees_teams_on_employees_id"
    t.index ["teams_id"], name: "index_employees_teams_on_teams_id"
  end

  create_table "note_bodies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.text "body"
    t.bigint "note_id"
    t.bigint "employees_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employees_id"], name: "index_note_bodies_on_employees_id"
    t.index ["note_id"], name: "index_note_bodies_on_note_id"
  end

  create_table "note_roadblocks", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.text "body"
    t.bigint "note_id"
    t.bigint "employees_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employees_id"], name: "index_note_roadblocks_on_employees_id"
    t.index ["note_id"], name: "index_note_roadblocks_on_note_id"
  end

  create_table "notes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "teams_id"
    t.bigint "employees_id"
    t.index ["employees_id"], name: "index_notes_on_employees_id"
    t.index ["teams_id"], name: "index_notes_on_teams_id"
  end

  create_table "solutions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.text "body"
    t.bigint "note_roadblock_id"
    t.bigint "users_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["note_roadblock_id"], name: "index_solutions_on_note_roadblock_id"
    t.index ["users_id"], name: "index_solutions_on_users_id"
  end

  create_table "teams", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.bigint "lead_id"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "users_id"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["users_id"], name: "index_user_roles_on_users_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "user_name"
    t.string "password_digest", default: "$2a$10$hgGZM1kdfcdVpsuLMaebe.V2Z1ewABL0.bHSpVqzONlh8utwSh/Iy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "comments", "notes"
  add_foreign_key "comments", "users", column: "users_id"
  add_foreign_key "employees", "users", column: "users_id"
  add_foreign_key "employees_teams", "employees", column: "employees_id"
  add_foreign_key "employees_teams", "teams", column: "teams_id"
  add_foreign_key "note_bodies", "employees", column: "employees_id"
  add_foreign_key "note_bodies", "notes"
  add_foreign_key "note_roadblocks", "employees", column: "employees_id"
  add_foreign_key "note_roadblocks", "notes"
  add_foreign_key "notes", "employees", column: "employees_id"
  add_foreign_key "notes", "teams", column: "teams_id"
  add_foreign_key "solutions", "note_roadblocks"
  add_foreign_key "solutions", "users", column: "users_id"
  add_foreign_key "user_roles", "users", column: "users_id"
end
