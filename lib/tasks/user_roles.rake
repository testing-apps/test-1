# frozen_string_literal: true

namespace :user_roles do
  desc 'TODO'
  task destroy_scrum: :environment do
    UserRole.where(role: 2).destroy_all
  end
end
