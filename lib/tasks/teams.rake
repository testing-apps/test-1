# frozen_string_literal: true

namespace :teams do
  desc 'TODO'
  task seed_team: :environment do
    Team.create(name: 'SnapIT', lead_id: nil, active: true)
  end
end
