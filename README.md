# Test App 1

This repo has the following bugs: 

- Login > Change password > Save > 404 red page
- Home page > No data populates the cards (should be notes)
- Pods > View Pod > Scroll down to button “Back to all Pods” > Doesn't work
- Login as non-admin employee > Append '/admin' to URL > Any user can see all employees list, should only be admin
- Create note > in body, type in a long string of characters with no space > Save note > View note you just created > CSS bug doesn't wrap long word

# Seeding

```ruby
rake db:create 
rake db:seed 
rake teams:seed_team  
```