# frozen_string_literal: true

class Solution < ApplicationRecord
  default_scope { order(created_at: :desc, id: :desc) }
  belongs_to :note_roadblock
end
