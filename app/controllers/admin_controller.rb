# frozen_string_literal: true

class AdminController < ApplicationController
  before_action :authorize
  before_action :admin_only, except: :index

  def new
    @teams = Team.active
  end

  def index
    @employees = Employee.active
    @admins = User.admin_users
  end

  def show
    @employee = Employee.find(params[:id])
  end

  def create
    # Loop through each field of input entered by the user params[:employee]
    # Strip the white space on either side of the input and check if input is empty after the strip
    if params[:employee].values.any?(&:blank?)
      # After stripping, if input is empty a redirect and an error flash saying all fields require input
      redirect_to new_admin_path, danger: 'All fields required'
    elsif Employee.find_by_email_address(params[:employee][:email_address].strip.downcase).present?
      redirect_to new_admin_path, danger: 'Email is already in use'
    else
      ActiveRecord::Base.transaction do
        # If input is stripped && not empty then the employee is saved to the DB with success message
        user = User.create(user_name: (params[:employee][:email_address]))

        UserRole.create(users_id: user.id)

        Employee.create(first_name: (params[:employee][:first_name]),
                        last_name: (params[:employee][:last_name]),
                        email_address: (params[:employee][:email_address]),
                        users_id: user.id)

        params[:team]&.each do |team_id|
          EmployeesTeam.create(employees_id: user.as_employee.id, teams_id: team_id.to_i, active: true)
        end
        redirect_to admin_index_path, success: 'Employee added'
      rescue StandardError
        render 'new', danger: 'Unable to save Employee'
        raise ActiveRecord::Rollback
      end
    end
  end

  def edit
    @employee = Employee.find(params[:id])
    @admin_users = User.admin_users
    @user = @employee.as_user
  end

  def update
    employee = Employee.find(params[:id])
    input = params[:employee_update]

    ActiveRecord::Base.transaction do
      employee.update(id: params[:id],
                      first_name: input[:first_name].presence || employee.first_name,
                      last_name: input[:last_name].presence || employee.last_name,
                      email_address: input[:email_address].presence || employee.email_address)

      employee.as_user.update(user_name: input[:email_address].presence || user.user_name)
      redirect_to admin_index_path, success: 'Employee updated'
    rescue StandardError
      render 'edit', danger: 'Unable to save updates'
      raise ActiveRecord::Rollback
    end
  end

  def set_admin
    employee = Employee.find(params[:id])

    UserRole.create(users_id: employee.users_id, role: 'admin')
    redirect_to admin_index_path, success: "#{employee} is an admin"
  end

  def remove_admin
    employee = Employee.find(params[:id])

    UserRole.where(users_id: employee.users_id, role: 'admin').destroy_all
    redirect_to admin_index_path, danger: "#{employee} is not an admin"
  end

  private

  def admin_only
    redirect_to '/', danger: 'Admin only' unless current_user.admin?
  end
end
