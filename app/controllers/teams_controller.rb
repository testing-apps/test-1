# frozen_string_literal: true

class TeamsController < ApplicationController
  before_action :authorize
  before_action :user_is_admin, only: %i[new create edit destroy update]

  def index
    @teams = Team.active
  end

  def show
    @team = Team.find(params[:id])
    @team_members = @team.members

    @lead = @team.lead_id.nil? ? nil : Employee.find(@team.lead_id)

    @team_notes = Note.where(teams_id: @team.id)
    @records = []

    EmployeesTeam.where(teams_id: @team.id).each do |record|
      employee = Employee.find(record.employees_id)

      @records << "#{employee} added on #{record.created_at.in_time_zone('America/Chicago').strftime('%a, %b %d, %Y')}" # rubocop:disable Metrics/LineLength
      unless record.active?
        @records << "#{employee} removed on #{record.updated_at.in_time_zone('America/Chicago').strftime('%a, %b %d, %Y')}" # rubocop:disable Metrics/LineLength
      end
    end
  end

  def new
    @header = 'Form to create new employee here'
    @employees = Employee.active
  end

  def create
    new_team = Team.create(
      name: params[:teams][:team_name],
      lead_id: params[:lead]
    )

    EmployeesTeam.create(
      employees_id: params[:lead],
      teams_id: new_team.id
    )
    team_members = params[:teams][:emp].delete_if { |_, value| value == '0' }
    team_members.each do |member_id, _|
      EmployeesTeam.create(employees_id: member_id, teams_id: new_team.id)
    end
    redirect_to teams_path, success: 'Team Created'
  end

  def edit
    @team = Team.find(params[:id])

    @lead = Employee.find(@team.lead_id) unless @team.lead_id.nil?

    @lead_options = Employee.active.delete_if { |employee| employee.id == @team.lead_id }

    @employees_not_on_team = Employee.active - @team.members
  end

  def update
    team = Team.find(params[:id])
    employees_to_remove = params[:remove]
    employees_to_add = params[:add]
    lead_id = params[:lead].presence || team.lead_id.to_s

    if employees_to_remove&.include?(lead_id)
      redirect_to(edit_team_path(team.id), danger: 'Lead cannot be removed from team') && return
    end

    team.add_member(lead_id).update(lead_id: lead_id)

    employees_to_add&.each { |employee| team.add_member(employee) }
    employees_to_remove&.each { |employee| team.remove_member(employee) }

    redirect_to team_path(params[:id]), success: 'Team edited'
  end

  def destroy
    team = Team.find(params[:id])
    team.update(active: false)

    EmployeesTeam.records_for(params[:id]).update_all(active: false)

    redirect_to teams_path, success: " #{team.name} removed "
  end

  private

  def user_is_admin
    redirect_to '/', danger: 'Admin only' unless current_user.admin?
  end
end
