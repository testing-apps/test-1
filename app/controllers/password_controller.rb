# frozen_string_literal: true

class PasswordController < ApplicationController
  before_action :authorize

  def edit; end
end
