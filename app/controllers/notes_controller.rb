# frozen_string_literal: true

class NotesController < ApplicationController
  before_action :authorize
  before_action :employee?, only: %i[new create edit destroy update]
  before_action :set_page, only: [:index]

  EMPLOYEE_NOT_PRESENT = 'Employee not present'

  def index
    notes_per_page = 10
    @total_notes = Note.all.length - 1
    @total_pages = (@total_notes.to_i / notes_per_page).ceil
    @notes = Note.limit(notes_per_page).offset(@current_page * notes_per_page)

    redirect_to notes_path if params[:page].to_i > @total_pages || params[:page].to_i.negative?
  end

  def set_page
    @current_page = params[:page].to_i || 0
  end

  def show
    @note = Note.find(params[:id])

    roadblocks = @note.roadblocks
    bodies = @note.bodies
    all_employees = roadblocks.map(&:employees_id) | bodies.map(&:employees_id)
    employees = Employee.find(all_employees.compact)

    @note_content = []
    employees&.each do |e|
      @note_content << {
        name: "#{e.first_name} #{e.last_name}",
        body: bodies.find_by(employees_id: e.id)&.body,
        roadblock: roadblocks.find_by(employees_id: e.id)&.body,
        roadblock_id: roadblocks.find_by(employees_id: e.id)&.id
      }
    end

    @team = Team.find(@note.teams_id)
    # for comments partial
    @users = User.all
  end

  def new
    @teams = Team.active
    @employees = Employee.active
  end

  def create
    if params[:team].nil?
      redirect_to new_note_url, warning: 'Select at least one team to create a note'
      return
    end

    teams = Team.find(params[:team])

    teams.each do |team|
      note = Note.create(title: team.name, teams_id: team.id, employees_id: current_user.as_employee.id)

      team.members.each do |team_member|
        input = params[:note_details][team_member.id.to_s]

        if input[:absent] == '1'
          input[:body] = EMPLOYEE_NOT_PRESENT
          input[:roadblock] = nil
        end

        NoteBody.create(
          body: input[:body],
          note_id: note.id,
          employees_id: team_member.id
        )

        NoteRoadblock.create(
          body: input[:roadblock],
          note_id: note.id,
          employees_id: team_member.id
        )
      end
    end
    redirect_to home_path, success: 'Note Saved'
  end

  def edit
    @note = Note.find(params[:id])
    @employees = Employee.all
    @note_bodies = NoteBody.where(note_id: params[:id])
    @note_roadblocks = NoteRoadblock.all.where(note_id: params[:id])
    @absent_employees = @note_bodies.where(body: EMPLOYEE_NOT_PRESENT).pluck(:employees_id)
  end

  def update
    note = Note.find(params[:id])
    note.update(title: params[:note_edit][:title])

    Employee.all.each do |e|
      input = params[:note_edit][e.id.to_s]
      next unless input.present?

      if input[:absent] == '1'
        NoteBody.find_by(note_id: params[:id], employees_id: e.id).update(body: EMPLOYEE_NOT_PRESENT)
        NoteRoadblock.find_by(note_id: params[:id], employees_id: e.id).update(body: nil)
      else
        NoteBody.find_by(note_id: params[:id], employees_id: e.id).update(body: input[:body])
        NoteRoadblock.find_by(note_id: params[:id], employees_id: e.id).update(body: input[:roadblock])
      end
    end
    redirect_to note, success: 'Update successful'
  end

  def destroy
    note = Note.find(params[:id])
    note.destroy
    redirect_to notes_path, success: 'Note deleted'
  end
end
